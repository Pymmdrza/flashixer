import requests, json, random


count = 0
filename = "addr.txt"
with open(filename, 'r') as f:
    for addr in f.readlines():
        addr = addr.strip('\n').strip()

        url = f"https://bitcoin.atomicwallet.io/api/v1/{addr}?page=1"
        req = requests.get(url).json()
        txs = dict(req)['transactions']
        for t in range(len(txs)):
            txid = dict(req)['transactions'][t]
            count += 1
            print(f"{count} # {addr}")
            print(f"{count} # {txid}")
            
